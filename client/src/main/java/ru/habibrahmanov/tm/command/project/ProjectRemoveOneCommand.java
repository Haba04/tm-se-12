package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectRemoveOneCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "remove project by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER ID PROJECT:");
        @NotNull final String projectId = serviceLocator.getScanner().nextLine();
        @NotNull final String userId = serviceLocator.getUserEndpoint().getCurrentUser().getId();
        serviceLocator.getProjectEndpoint().removeProject(serviceLocator.getCurrentSession(), projectId);
        serviceLocator.getTaskEndpoint().removeAllTask(serviceLocator.getCurrentSession(), projectId);
        System.out.println("PROJECT REMOVED SUCCESSFULLY");
    }
}
