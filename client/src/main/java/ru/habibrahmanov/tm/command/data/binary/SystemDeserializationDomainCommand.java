package ru.habibrahmanov.tm.command.data.binary;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemDeserializationDomainCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "deserialization-domain";
    }

    @Override
    public String getDescription() {
        return "loading a domain using serialization";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().deserializationDomain(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}
