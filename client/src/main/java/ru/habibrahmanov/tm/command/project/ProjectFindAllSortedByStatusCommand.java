package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Project;

import java.util.List;

public class ProjectFindAllSortedByStatusCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-sort-status";
    }

    @Override
    public String getDescription() {
        return "project sort by status";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS SORTED BY DATE END]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllSortedByStatusProject(serviceLocator.getCurrentSession());
        for (Project project : projectList) {
            System.out.println(project);
            System.out.println();
        }
    }
}
