package ru.habibrahmanov.tm.command.data.xml;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemSaveJaxbXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jaxb-xml";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jaxb technology in xml transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().saveJaxbXml(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}
