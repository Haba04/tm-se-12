package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Project;

import java.util.List;

public class ProjectFindAllSortedByDateBeginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-sort-data-begin";
    }

    @Override
    public String getDescription() {
        return "project find all sorted by data begin command";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS SORTED BY DATE BEGIN]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectEndpoint().findAllSortedByDateBeginProject(serviceLocator.getCurrentSession());
        for (Project project : projectList) {
            System.out.println(project);
            System.out.println();
        }
    }
}
