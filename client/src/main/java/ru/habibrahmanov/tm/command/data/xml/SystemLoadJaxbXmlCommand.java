package ru.habibrahmanov.tm.command.data.xml;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemLoadJaxbXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "load-jaxb-xml";
    }

    @Override
    public String getDescription() {
        return "loading the subject area using externalization and jaxb technology in xml transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().loadJaxbXml(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}