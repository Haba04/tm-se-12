package ru.habibrahmanov.tm.command.data.xml;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class SystemSaveJacksonXmlCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "save-jackson-xml";
    }

    @Override
    public String getDescription() {
        return "saving the subject area using externalization and jackson technology in xml transport format";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getDomainEndpoint().saveJacksonXml(serviceLocator.getCurrentSession());
        System.out.println("OK");
    }
}
