package ru.habibrahmanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserEditProfileCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "edit-profile";
    }

    @Override
    public String getDescription() {
        return "user edit profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("EDIT PROFILE");
        System.out.println("ENTER NEW LOGIN");
        @Nullable final String login = serviceLocator.getScanner().nextLine();
        serviceLocator.getUserEndpoint().editProfile(serviceLocator.getCurrentSession(), login);
    }
}
