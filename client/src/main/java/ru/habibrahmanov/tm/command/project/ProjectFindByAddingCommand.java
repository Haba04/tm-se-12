package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.api.Project;

import java.util.List;

public class ProjectFindByAddingCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-find-adding";
    }

    @Override
    public String getDescription() {
        return "shows all projects in the order they were added";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY ADDING]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectEndpoint().findByAddingProject(serviceLocator.getCurrentSession());
        for (Project project : projectList) {
            System.out.println(project);
        }
    }
}
