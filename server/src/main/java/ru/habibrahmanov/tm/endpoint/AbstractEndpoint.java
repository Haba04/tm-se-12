package ru.habibrahmanov.tm.endpoint;

import ru.habibrahmanov.tm.api.EndpointLocator;

public abstract class AbstractEndpoint {
    EndpointLocator endpointLocator;

    public void setEndpointLocator(EndpointLocator endpointLocator) {
        this.endpointLocator = endpointLocator;
    }
}
