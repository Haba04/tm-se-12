package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.Status;

import java.sql.ResultSet;
import java.util.Date;
import java.util.List;

public interface IProjectRepository {
    @Nullable Project fetch(@Nullable ResultSet row);

    void persist(@NotNull Project project);

    @Nullable List<Project> findAll(@NotNull String userId);

    @Nullable List<Project> findAll();

    void update(
            @NotNull String userId, @NotNull String projectId, @NotNull String name, @NotNull String description,
            @NotNull String status, @NotNull Date dateBegin, @NotNull Date dateEnd
    );

    @NotNull List<Project> searchByString (@NotNull String projectId, @NotNull String string);

    @Nullable Project findOne(@NotNull String projectId, @NotNull String userId);

    boolean removeAll(@NotNull String userId);

    boolean remove(@NotNull String projectId, @NotNull String userId);

    void merge(@NotNull Project project);
}
