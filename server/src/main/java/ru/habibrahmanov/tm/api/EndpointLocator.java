package ru.habibrahmanov.tm.api;

import java.util.Scanner;

public interface EndpointLocator {
    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    Scanner getScanner();

    IProjectEndpoint getProjectEndpoint();

    ITaskEndpoint getTaskEndpoint();

    IUserEndpoint getUserEndpoint();

    ISessionEndpoint getSessionEndpoint();

    ISessionService getSessionService();
}
