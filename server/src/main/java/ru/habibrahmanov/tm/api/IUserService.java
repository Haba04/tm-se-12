package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface IUserService {
    void registryAdmin(
            @Nullable String login, @Nullable String password, @Nullable String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    void registryUser(@Nullable User user) throws IllegalArgumentException;

    void updatePassword(
            @Nullable User currentUser, @Nullable String curPassword, @Nullable String newPassword,
            @Nullable String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    @Nullable User login(
            @Nullable String login, @Nullable String password
    ) throws IncorrectValueException, UnsupportedEncodingException, NoSuchAlgorithmException;

    @Nullable User viewProfile();

    @NotNull List<User> findAll() throws ListIsEmptyExeption;

    @NotNull User findOne(@Nullable String userId) throws IncorrectValueException;

    @NotNull User findByLogin(@Nullable String login) throws ListIsEmptyExeption;

    void editProfile(@Nullable String newLogin) throws IncorrectValueException;

    void logout();

    boolean isAuth();

    @Nullable User getCurrentUser();
}
