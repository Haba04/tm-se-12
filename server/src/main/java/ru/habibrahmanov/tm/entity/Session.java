package ru.habibrahmanov.tm.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@NoArgsConstructor
public class Session implements Cloneable {
    private String id = UUID.randomUUID().toString();
    private String userId;
    private Long timeStamp = System.currentTimeMillis();
    private String signature;

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Override
    @NotNull
    public String toString() {
        return "Id \"" + id + "\"" +
                "\nUser Id = " + userId +
                "\nTime stamp = " + timeStamp +
                "\n";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
