package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.comporator.TaskDateBeginComparator;
import ru.habibrahmanov.tm.comporator.TaskDateEndComparator;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.repository.TaskRepository;
import ru.habibrahmanov.tm.util.DbConnectUtil;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public final class TaskService extends AbstractService implements ITaskService {

    @Override
    public void persist(@Nullable final Task task) throws IncorrectValueException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (task == null) throw new IncorrectValueException();
        taskRepository.persist(task);
    }

    @Override
    public void insert(
            @Nullable final String projectId, @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        taskRepository.persist(
                new Task(UUID.randomUUID().toString(), projectId, userId, name,
                        description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd))
        );
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String taskId, @Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findOne(taskId, userId) == null) throw new ListIsEmptyExeption();
        return taskRepository.findOne(taskId, userId);
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findAll(userId).isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<Task> findAll() throws ListIsEmptyExeption {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (taskRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.findAll();
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws IncorrectValueException, RemoveFailedException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        if (!taskRepository.remove(userId, taskId)) throw new RemoveFailedException();
        taskRepository.remove(userId, taskId);
    }

    @Override
    public void removeAll(@Nullable final String userId, @Nullable final String projectId) throws IncorrectValueException, RemoveFailedException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (!taskRepository.removeAll(userId, projectId)) throw new RemoveFailedException();
        taskRepository.removeAll(userId, projectId);
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description,
                       @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (id == null || id.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        taskRepository.update(userId, id, name, description, status, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @Override
    public void merge(
            @Nullable final String id, @Nullable final String projectId, @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (id == null || id.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        taskRepository.merge(new Task(id, projectId, userId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByDateBegin(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Task> taskList = findAll(userId);
        Collections.sort(taskList, new TaskDateBeginComparator());
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByDateEnd(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Task> projectList = findAll(userId);
        Collections.sort(projectList, new TaskDateEndComparator());
        return projectList;
    }

    @NotNull
    @Override
    public List<Task> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Task> taskList = findAll(userId);
        Collections.sort(taskList);
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> searchByString (
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        @Nullable final TaskRepository taskRepository = new TaskRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.searchByString(userId, string);
    }
}
