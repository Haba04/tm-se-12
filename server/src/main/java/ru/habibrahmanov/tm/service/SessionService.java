package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ISessionRepository;
import ru.habibrahmanov.tm.api.ISessionService;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.entity.Session;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.SessionIsNotValidException;
import ru.habibrahmanov.tm.repository.SessionRepository;
import ru.habibrahmanov.tm.repository.UserRepository;
import ru.habibrahmanov.tm.util.DbConnectUtil;
import ru.habibrahmanov.tm.util.HashUtil;
import ru.habibrahmanov.tm.util.SignatureUtil;

public class SessionService implements ISessionService {

    @Override
    public void validate(@Nullable final Session session) throws SessionIsNotValidException {
        if (session == null) throw new SessionIsNotValidException();
        if (session.getId() == null || session.getId().isEmpty()) throw new SessionIsNotValidException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new SessionIsNotValidException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new SessionIsNotValidException();
        @Nullable final String currentSignature = session.getSignature();
        @Nullable final Session tempSession = session.clone();
        tempSession.setSignature(null);
        @Nullable final String newSignature = SignatureUtil.sign(tempSession, "qweasd", 7);
        if (!currentSignature.equals(newSignature)) throw new SessionIsNotValidException();
        @Nullable final Long timeStamp = session.getTimeStamp();
        @Nullable final Long currentTimeStamp = System.currentTimeMillis();
        if (currentTimeStamp - timeStamp > 1800000) throw new SessionIsNotValidException("Session is time out");
    }

    @Override
    public Session open(@Nullable final String login, @Nullable final String password) throws IncorrectValueException {
        @Nullable final SessionRepository sessionRepository = new SessionRepository(DbConnectUtil.getConnection());
        @Nullable final UserRepository userRepository = new UserRepository(DbConnectUtil.getConnection());
        if (login == null || login.isEmpty()) throw new IllegalArgumentException();
        if (password == null || password.isEmpty()) throw new IllegalArgumentException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new IncorrectValueException("USER DON'T EXIST");
        @Nullable final String currentPassword = HashUtil.md5(password);
        if (currentPassword == null || currentPassword.isEmpty()) throw new IncorrectValueException("WRONG PASSWORD");
        if (!user.getPassword().equals(currentPassword)) throw new IncorrectValueException("WRONG PASSWORD");
        @Nullable final Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final String signature = SignatureUtil.sign(session, "qweasd", 7);
        session.setSignature(signature);
        sessionRepository.persist(session);
        return session;
    }

    @Override
    public void close(@Nullable final Session session) throws SessionIsNotValidException {
        @Nullable final SessionRepository sessionRepository = new SessionRepository(DbConnectUtil.getConnection());
        if (session == null) throw new SessionIsNotValidException();
        sessionRepository.remove(session);
    }
}
