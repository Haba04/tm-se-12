package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.comporator.ProjectDateBeginComparator;
import ru.habibrahmanov.tm.comporator.ProjectDateEndComparator;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.repository.ProjectRepository;
import ru.habibrahmanov.tm.util.DbConnectUtil;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public final class ProjectService extends AbstractService implements IProjectService {

    @Override
    public void insert(
            @Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
            @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        projectRepository.persist(new Project(UUID.randomUUID().toString(), userId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }

    @Override
    public void persist(@Nullable final Project project) throws IncorrectValueException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (project == null) throw new IncorrectValueException();
        projectRepository.persist(project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.findAll(userId).isEmpty()) {
            throw new ListIsEmptyExeption();
        }
        return projectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll() throws ListIsEmptyExeption {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (projectRepository.findAll().isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.findAll();
    }

    @Override
    public void removeAll(@Nullable final String userId) throws IncorrectValueException, RemoveFailedException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (!projectRepository.removeAll(userId)) throw new RemoveFailedException();
        projectRepository.removeAll(userId);
    }

    @Override
    public void remove(@Nullable final String projectId, @Nullable final String userId) throws IncorrectValueException, RemoveFailedException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (!projectRepository.remove(projectId, userId)) throw new RemoveFailedException();
        projectRepository.remove(projectId, userId);
    }

    @Override
    public void update(
            @Nullable final String userId, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String status, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        projectRepository.update(userId, projectId, name, description, status, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @Override
    public void merge(
            @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
            @Nullable final String dateBegin, @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty())  throw new IncorrectValueException();
        if (description == null || description.isEmpty())  throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty())  throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty())  throw new IncorrectValueException();
        projectRepository.merge(new Project(projectId, userId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByDateBegin(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList, new ProjectDateBeginComparator());
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByDateEnd(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList, new ProjectDateEndComparator());
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList);
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> searchByString (
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        @Nullable final ProjectRepository projectRepository = new ProjectRepository(DbConnectUtil.getConnection());
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.searchByString(userId, string);
    }
}
