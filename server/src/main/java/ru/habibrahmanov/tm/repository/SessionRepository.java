package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.api.ISessionRepository;
import ru.habibrahmanov.tm.entity.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(Connection connection) {
        super(connection);
    }

    @Override
    public void persist(Session session) {
        try {
            @NotNull final String query = "INSERT INTO app_session(id, user_id, time_stamp, signature) " +
                    "VALUES (?, ?, ?, ?)";
            @NotNull PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setLong(3, session.getTimeStamp());
            statement.setString(4, session.getSignature());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Session session) {
        try {
            @NotNull final String query = "DELETE FROM app_session WHERE id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, session.getId());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
