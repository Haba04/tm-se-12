package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T> {

    Connection connection;

    public AbstractRepository(Connection connection) {
        this.connection = connection;
    }

    public abstract void persist(final T t);

    @NotNull
    final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
