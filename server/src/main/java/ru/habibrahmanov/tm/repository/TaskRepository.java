package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.comporator.TaskDateBeginComparator;
import ru.habibrahmanov.tm.comporator.TaskDateEndComparator;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.enumeration.FieldConst;
import ru.habibrahmanov.tm.enumeration.Status;
import ru.habibrahmanov.tm.util.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {
    public TaskRepository(Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        try {
            task.setId(row.getString(FieldConst.ID.name()));
            task.setName(row.getString(FieldConst.NAME.name()));
            task.setDescription(row.getString(FieldConst.DESCRIPTION.name()));
            task.setDateBegin(row.getDate(String.valueOf(FieldConst.DATE_BEGIN)));
            task.setDateEnd(row.getDate(String.valueOf(FieldConst.DATE_END)));
            return task;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void persist(@NotNull final Task task) {
        try {
            @NotNull final String query = "INSERT INTO app_task(id, project_id, user_id, name, description, date_begin, date_end, status) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            @NotNull PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, task.getId());
            statement.setString(2, task.getProjectId());
            statement.setString(3, task.getUserId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setDate(6, DateUtil.utilToSqlDate(task.getDateBegin()));
            statement.setDate(7, DateUtil.utilToSqlDate(task.getDateEnd()));
            statement.setString(8, task.getStatus().displayName());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id, @NotNull final String userId) {
        try {
            @NotNull final String query = "SELECT * FROM app_project WHERE id = ? AND user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            statement.setString(2, userId);
            statement.execute();
            statement.close();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull Task task = new Task();
            while (resultSet.next())
                task = fetch(resultSet);
            return task;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        try {
            @NotNull final String query = "SELECT * FROM app_task WHERE user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, userId);
            statement.execute();
            statement.close();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> result = new ArrayList<>();
            while (resultSet.next())
                result.add(fetch(resultSet));
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        try {
            @NotNull final String query = "SELECT * FROM app_task";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Task> result = new ArrayList<>();
            while (resultSet.next())
                result.add(fetch(resultSet));
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String id) {
        try {
            @NotNull final String query = "DELETE FROM app_task WHERE id = ? AND user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            statement.setString(2, userId);
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean removeAll(@NotNull final String userId, @NotNull final String projectId) {
        try {
            @NotNull final String query = "DELETE FROM app_task WHERE user_id = ? AND project_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, userId);
            statement.setString(2, projectId);
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description,
                       @NotNull final String status, @NotNull final Date dateBegin, @NotNull final Date dateEnd) {
        try {
            @NotNull final String query = "UPDATE app_task SET name = ?, description = ?, status = ?, " +
                    "date_begin = ?, date_end = ? WHERE id = ? AND user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            statement.setString(2, description);
            switch (status) {
                case ("inprogress") :
                    statement.setString(3, Status.INPROGRESS.displayName());
                    break;
                case ("planned") :
                    statement.setString(3, Status.PLANNED.displayName());
                    break;
                case ("ready") :
                    statement.setString(3, Status.READY.displayName());
                    break;
            }
            statement.setString(4, dateFormat.format(dateBegin));
            statement.setString(5, dateFormat.format(dateEnd));
            statement.setString(6, id);
            statement.setString(7, userId);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@NotNull final Task task) {
        if (task.getId() == null) {
            persist(task);
        }
        update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getStatus().displayName(),
                task.getDateBegin(), task.getDateEnd());
    }

    @NotNull
    @Override
    public List<Task> searchByString (@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Task> taskList = findAll(userId);
        @NotNull final List<Task> taskListWithMatches = new ArrayList<>();
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getName().contains(string) || taskList.get(i).getDescription().contains(string)) {
                taskListWithMatches.add(taskList.get(i));
            }
        }
        return taskListWithMatches;
    }
}