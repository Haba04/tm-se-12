package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.FieldConst;
import ru.habibrahmanov.tm.util.DbConnectUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    public UserRepository(Connection connection) {
        super(connection);
    }

    @Override
    @Nullable
    public User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        try {
            user.setId(row.getString(FieldConst.ID.name()));
            user.setLogin(row.getString(FieldConst.LOGIN.name()));
            user.setPassword(row.getString(FieldConst.PASSWORD.name()));
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void persist(@NotNull final User user) {
        try {
            @NotNull final String query = "INSERT INTO app_user(id, login, password, role) " +
                    "VALUES (?, ?, ?, ?)";
            @NotNull PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setString(4, user.getRole().displayName());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String userId) {
        try {
            @NotNull final String query = "SELECT * FROM app_user WHERE id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, userId);
            statement.execute();
            statement.close();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull User user = new User();
            while (resultSet.next())
                user = fetch(resultSet);
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        try {
            @NotNull final String query = "SELECT * FROM app_user WHERE login = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, login);
            statement.execute();
            statement.close();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull User user = new User();
            while (resultSet.next())
                user = fetch(resultSet);
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public List<User> findAll() {
        try {
            @NotNull final String query = "SELECT * FROM app_user";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<User> result = new ArrayList<>();
            while (resultSet.next())
                result.add(fetch(resultSet));
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void removeOne(@NotNull final String login) {
        try {
            @NotNull final String query = "DELETE FROM app_user WHERE login = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, login);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeAll() {
        try {
            @NotNull final String query = "DELETE FROM app_user";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String login) {
        try {
            @NotNull final String query = "UPDATE app_user SET login = ? WHERE id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, login);
            statement.setString(2, userId);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@NotNull final User user) {
        if (user.getId() == null) {
            persist(user);
        }
        update(user.getId(), user.getLogin());
    }
}
