package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.comporator.ProjectDateBeginComparator;
import ru.habibrahmanov.tm.comporator.ProjectDateEndComparator;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.enumeration.FieldConst;
import ru.habibrahmanov.tm.enumeration.Status;
import ru.habibrahmanov.tm.util.DateUtil;
import ru.habibrahmanov.tm.util.DbConnectUtil;

import java.sql.*;
import java.util.Date;
import java.util.*;


public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    @Override
    @Nullable
    public Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        try {
            project.setId(row.getString(FieldConst.ID.name()));
            project.setName(row.getString(FieldConst.NAME.name()));
            project.setDescription(row.getString(FieldConst.DESCRIPTION.name()));
            project.setDateBegin(row.getDate(String.valueOf(FieldConst.DATE_BEGIN)));
            project.setDateEnd(row.getDate(String.valueOf(FieldConst.DATE_END)));
            return project;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void persist(@NotNull final Project project) {
        try {
            @NotNull final String query = "INSERT INTO app_project(id, user_id, name, description, date_begin, date_end, status) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?)";
            @NotNull PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setDate(5, DateUtil.utilToSqlDate(project.getDateBegin()));
            statement.setDate(5, DateUtil.utilToSqlDate(project.getDateBegin()));
            statement.setDate(6, DateUtil.utilToSqlDate(project.getDateEnd()));
            statement.setString(7, project.getStatus().displayName());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        try {
            @NotNull final String query = "SELECT * FROM app_project WHERE user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, userId);
            statement.execute();
            statement.close();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Project> result = new ArrayList<>();
            while (resultSet.next())
                result.add(fetch(resultSet));
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        try {
            @NotNull final String query = "SELECT * FROM app_project";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull final List<Project> result = new ArrayList<>();
            while (resultSet.next())
                result.add(fetch(resultSet));
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String projectId, @NotNull final String userId) {
        try {
            @NotNull final String query = "SELECT * FROM app_project WHERE id = ? AND user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, projectId);
            statement.setString(2, userId);
            statement.execute();
            statement.close();
            @NotNull final ResultSet resultSet = statement.executeQuery();
            @NotNull Project project = new Project();
            while (resultSet.next())
                project = fetch(resultSet);
            return project;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean removeAll(@NotNull final String userId) {
        try {
            @NotNull final String query = "DELETE FROM app_project WHERE user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, userId);
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean remove(@NotNull final String projectId, @NotNull final String userId) {
        try {
            @NotNull final String query = "DELETE FROM app_project WHERE id = ? AND user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, projectId);
            statement.setString(2, userId);
            statement.execute();
            statement.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void merge(@NotNull final Project project) {
        if (project.getId() == null) {
            persist(project);
        }
        update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getStatus().displayName(),
                project.getDateBegin(), project.getDateEnd());
    }

    @Override
    public void update(
            @NotNull final String userId, @NotNull final String projectId, @NotNull final String name, @NotNull final String description,
            @NotNull final String status, @NotNull final Date dateBegin, @NotNull final Date dateEnd
    ) {
        try {
            @NotNull final String query = "UPDATE app_project SET name = ?, description = ?, status = ?, " +
                    "date_begin = ?, date_end = ? WHERE id = ? AND user_id = ?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            statement.setString(2, description);
            switch (status) {
                case ("inprogress") :
                    statement.setString(3, Status.INPROGRESS.displayName());
                    break;
                case ("planned") :
                    statement.setString(3, Status.PLANNED.displayName());
                    break;
                case ("ready") :
                    statement.setString(3, Status.READY.displayName());
                    break;
            }
            statement.setString(4, dateFormat.format(dateBegin));
            statement.setString(5, dateFormat.format(dateEnd));
            statement.setString(6, projectId);
            statement.setString(7, userId);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public List<Project> searchByString(@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Project> projectList = findAll(userId);
        @NotNull final List<Project> projectListWithMatches = new ArrayList<>();
        for (int i = 0; i < projectList.size(); i++) {
            if (projectList.get(i).getName().contains(string) || projectList.get(i).getDescription().contains(string)) {
                projectListWithMatches.add(projectList.get(i));
            }
        }
        return projectListWithMatches;
    }
}