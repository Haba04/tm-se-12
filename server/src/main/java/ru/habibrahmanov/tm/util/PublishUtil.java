package ru.habibrahmanov.tm.util;

import ru.habibrahmanov.tm.endpoint.AbstractEndpoint;
import javax.xml.ws.Endpoint;
import java.util.Set;

public class PublishUtil {

    public static void publish(Set<AbstractEndpoint> endpoints) {
        for (AbstractEndpoint abstractEndpoint : endpoints) {
            publish(abstractEndpoint);
        }
    }

    private static void publish(AbstractEndpoint endpoint) {
        String endpointName = endpoint.getClass().getSimpleName();
        String link = "http://localhost:8080/" + endpointName + "?wsdl";
        System.out.println(endpointName + " : " + link);
        Endpoint.publish(link, endpoint);
    }
}
