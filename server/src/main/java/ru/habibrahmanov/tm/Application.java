package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.bootstrap.Bootstrap;
import ru.habibrahmanov.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class Application {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}